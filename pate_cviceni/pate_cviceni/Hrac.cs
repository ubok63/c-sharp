﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pate_cviceni
{
    public class Hrac
    {
        public string Jmeno { get; set; }
        public FotbalovyKlub Klub { get; set; }
        public int GolPocet { get; set; }

        public Hrac(string jmeno, FotbalovyKlub klub, int golPocet)
        {
            this.Jmeno = jmeno;
            this.Klub = klub;
            this.GolPocet = golPocet;
        }

        public override string ToString()
        {
            return Jmeno + "  " + FotbalovyKlubInfo.DejNazev(Klub) + "  " + GolPocet;
        }

    }
}
