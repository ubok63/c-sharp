﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pate_cviceni
{
    public class Seznam : IEnumerable, IEnumerator, ICollection, IList
    {
        Prvek prvni;
        Prvek posledni;
        int pocet;

        int pozice = -1;

        public object this[int index] { get => PrvekVSeznamuOfIndex(index).Data; set => PrvekVSeznamuOfIndex(index).Data = value; }

        public int Count => pocet;

        public object SyncRoot => this;

        public bool IsSynchronized => false;

        public bool IsReadOnly => false;

        public bool IsFixedSize => false;

        public int Add(object value)
        {
            Prvek prvek = new Prvek(value);
            if (prvni == null && posledni == null)
            {
                prvni = posledni = prvek;
            }
            else if (prvni == posledni)
            {
                prvni.Dalsi = posledni = prvek;
                prvek.Predchozi = prvni;
            }
            else
            {
                posledni.Dalsi = prvek;
                prvek.Predchozi = posledni;
                posledni = prvek;
            }
            pocet++;
            return Count - 1;
        }

        public void Clear()
        {
            prvni = posledni = null;
            pocet = 0;
        }

        public bool Contains(object value)
        {
            return IndexOf(value) != -1;
        }

        public void CopyTo(Array array, int index)
        {
            if (Count > 0)
            {
                Prvek prvek = prvni;
                for (int i = index; i < Count; i++)
                {
                    if (prvek != null)
                    {
                        array.SetValue(prvek.Data, i - index);
                        prvek = prvek.Dalsi;
                    }
                }
            }
        }

        public IEnumerator GetEnumerator()
        {
            return (IEnumerator)this;
        }

        public bool MoveNext()
        {
            pozice++;
            return (pozice < pocet);
        }
        public void Reset()
        {
            pozice = -1;
        }

        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }

        public Object Current
        {
            get
            {
                try
                {
                    return PrvekVSeznamuOfIndex(pozice).Data;
                }
                catch (IndexOutOfRangeException)
                {
                    throw new InvalidOperationException();
                }
            }
        }

        public int IndexOf(object value)
        {
            int index;
            PrvekVSeznamuOf(value, out index);
            return index;
        }

        public void Insert(int index, object value)
        {

            if (Count > 0)
            {
                Prvek prvek = PrvekVSeznamuOfIndex(index);
                Prvek prvekKVlozeni = new Prvek(value);
                if (prvek != null)
                {
                    prvekKVlozeni.Predchozi = prvek.Predchozi;
                    if (prvek == prvni)
                    {
                        prvni = prvekKVlozeni;
                    }
                    else
                    {
                        prvek.Predchozi.Dalsi = prvekKVlozeni;
                    }
                    prvekKVlozeni.Dalsi = prvek;
                    prvek.Predchozi = prvekKVlozeni;
                    pocet++;
                }
                else
                {
                    Add(value);
                }
            }
            else if (index == 0)
            {
                Add(value);
            }
        }

        public void Remove(object value)
        {
            if (Contains(value) && value != null)
            {
                int index;
                Prvek prvek = PrvekVSeznamuOf(value, out index);

                if (prvek.Predchozi != null)
                {
                    prvek.Predchozi.Dalsi = prvek.Dalsi;
                }
                else
                {
                    prvni = prvek.Dalsi;
                }

                if (prvek.Dalsi != null)
                {
                    prvek.Dalsi.Predchozi = prvek.Predchozi;
                }
                else
                {
                    posledni = prvek.Predchozi;
                }

                if (prvni != null && posledni == null)
                {
                    posledni = prvni;
                }
                else if (prvni == null && posledni != null)
                {
                    prvni = posledni;
                }
                pocet--;
            }
        }

        public void RemoveAt(int index)
        {
            if (index < pocet)
            {
                Remove(PrvekVSeznamuOfIndex(index).Data);
                pocet--;
            }
                
        }

        private Prvek PrvekVSeznamuOf(object value, out int index)
        {
            if (Count > 0)
            {
                Prvek prvek = prvni;
                for (int i = 0; i < Count; i++)
                {
                    if (prvek != null)
                    {
                        if (prvek.Data.Equals(value))
                        {
                            index = i;
                            return prvek;
                        }
                        prvek = prvek.Dalsi;
                    }
                }
            }
            index = -1;
            return null;
        }

        private Prvek PrvekVSeznamuOfIndex(int index)
        {

            if (index <= Count - 1)
            {
                Prvek prvek = prvni;
                for (int i = 0; i < Count; i++)
                {
                    if (i == index)
                    {
                        return prvek;
                    }
                    prvek = prvek.Dalsi;
                }
            }
            return null;
        }







        private class Prvek
        {
            public object Data { get; set; }
            public Prvek Dalsi { get; set; }
            public Prvek Predchozi { get; set; }

            public Prvek(object Data)
            {
                this.Data = Data;
                Dalsi = Predchozi = null;
            }
        }
    }
}
