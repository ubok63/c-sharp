﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pate_cviceni
{
    

    public class Hraci
    {
        public Seznam seznam { get; set; }
        public delegate void PocetHracuChangeEventHandler();
        public event PocetHracuChangeEventHandler PocetZmenen;

        public Hraci()
        {
            seznam = new Seznam();
        }

        public void Vymaz(Hrac h)
        {
            seznam.Remove(h);
            OnPocetHracuChanged();
        }

        public void Pridej(Hrac hrac)
        {
            seznam.Add(hrac);
            OnPocetHracuChanged();
        }

        public void NajdiNejlepsiKluby(out string[] kluby, out int golPocet)
        {
            int pocetKlubu = 0;
            var t = new Dictionary<FotbalovyKlub, int>();

            t.Add(FotbalovyKlub.Porto, 0);
            t.Add(FotbalovyKlub.Arsenal, 0);
            t.Add(FotbalovyKlub.Real, 0);
            t.Add(FotbalovyKlub.Chelsea, 0);
            t.Add(FotbalovyKlub.Barcelona, 0);

            foreach (Hrac h in seznam)
            {
                    switch (h.Klub)
                    {
                        case FotbalovyKlub.Porto:
                            t[FotbalovyKlub.Porto] += h.GolPocet;
                            break;
                        case FotbalovyKlub.Arsenal:
                            t[FotbalovyKlub.Arsenal] += h.GolPocet;
                            break;
                        case FotbalovyKlub.Real:
                            t[FotbalovyKlub.Real] += h.GolPocet;
                            break;
                        case FotbalovyKlub.Chelsea:
                            t[FotbalovyKlub.Chelsea] += h.GolPocet;
                            break;
                        case FotbalovyKlub.Barcelona:
                            t[FotbalovyKlub.Barcelona] += h.GolPocet;
                            break;
                    }
                
            }
            seznam.Reset();
            golPocet = t.Values.Max();
            foreach(KeyValuePair<FotbalovyKlub, int> k in t)
            {
                if(k.Value == golPocet)
                {
                    pocetKlubu++;
                }
            }
            kluby = new String[pocetKlubu];
            int x = 0;
            foreach (KeyValuePair<FotbalovyKlub, int> k in t)
            {
                if (k.Value == golPocet)
                {
                    kluby[x] = FotbalovyKlubInfo.DejNazev(k.Key);
                    x++;
                }
            }
        }

        public Hrac this[int index]
        {
            get => (Hrac)seznam[index];
        }

        private void OnPocetHracuChanged()
        {
            PocetZmenen?.Invoke();
        }

    }
}
