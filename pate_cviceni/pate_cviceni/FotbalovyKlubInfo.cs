﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace pate_cviceni
{
    public enum FotbalovyKlub
    {
        None,
        Porto,
        Arsenal,
        Real,
        Chelsea,
        Barcelona
    }
    static class FotbalovyKlubInfo
    {
        public static int Pocet { get; } = 6;
        public static string DejNazev(FotbalovyKlub f)
        {
            return Enum.GetName(typeof(FotbalovyKlub), f);
        }
        public static FotbalovyKlub DejTyp(string s)
        {
            switch (s)
            {
                case "None":
                    return FotbalovyKlub.None;
                case "Porto":
                    return FotbalovyKlub.Porto;
                case "Arsenal":
                    return FotbalovyKlub.Arsenal;
                case "Real":
                    return FotbalovyKlub.Real;
                case "Chelsea":
                    return FotbalovyKlub.Chelsea;
                case "Barcelona":
                    return FotbalovyKlub.Barcelona;
                default:
                    return FotbalovyKlub.None;
            }
        }
    }
}
