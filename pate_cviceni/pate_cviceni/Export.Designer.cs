﻿namespace pate_cviceni
{
    partial class Export
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbNone = new System.Windows.Forms.CheckBox();
            this.cbPorto = new System.Windows.Forms.CheckBox();
            this.cbArsenal = new System.Windows.Forms.CheckBox();
            this.cbReal = new System.Windows.Forms.CheckBox();
            this.cbChelsea = new System.Windows.Forms.CheckBox();
            this.cbBarcelona = new System.Windows.Forms.CheckBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cbNone
            // 
            this.cbNone.AutoSize = true;
            this.cbNone.Location = new System.Drawing.Point(13, 13);
            this.cbNone.Name = "cbNone";
            this.cbNone.Size = new System.Drawing.Size(52, 17);
            this.cbNone.TabIndex = 0;
            this.cbNone.Text = "None";
            this.cbNone.UseVisualStyleBackColor = true;
            // 
            // cbPorto
            // 
            this.cbPorto.AutoSize = true;
            this.cbPorto.Location = new System.Drawing.Point(13, 37);
            this.cbPorto.Name = "cbPorto";
            this.cbPorto.Size = new System.Drawing.Size(51, 17);
            this.cbPorto.TabIndex = 1;
            this.cbPorto.Text = "Porto";
            this.cbPorto.UseVisualStyleBackColor = true;
            // 
            // cbArsenal
            // 
            this.cbArsenal.AutoSize = true;
            this.cbArsenal.Location = new System.Drawing.Point(13, 61);
            this.cbArsenal.Name = "cbArsenal";
            this.cbArsenal.Size = new System.Drawing.Size(61, 17);
            this.cbArsenal.TabIndex = 2;
            this.cbArsenal.Text = "Arsenal";
            this.cbArsenal.UseVisualStyleBackColor = true;
            // 
            // cbReal
            // 
            this.cbReal.AutoSize = true;
            this.cbReal.Location = new System.Drawing.Point(13, 85);
            this.cbReal.Name = "cbReal";
            this.cbReal.Size = new System.Drawing.Size(48, 17);
            this.cbReal.TabIndex = 3;
            this.cbReal.Text = "Real";
            this.cbReal.UseVisualStyleBackColor = true;
            // 
            // cbChelsea
            // 
            this.cbChelsea.AutoSize = true;
            this.cbChelsea.Location = new System.Drawing.Point(13, 109);
            this.cbChelsea.Name = "cbChelsea";
            this.cbChelsea.Size = new System.Drawing.Size(64, 17);
            this.cbChelsea.TabIndex = 4;
            this.cbChelsea.Text = "Chelsea";
            this.cbChelsea.UseVisualStyleBackColor = true;
            // 
            // cbBarcelona
            // 
            this.cbBarcelona.AutoSize = true;
            this.cbBarcelona.Location = new System.Drawing.Point(13, 133);
            this.cbBarcelona.Name = "cbBarcelona";
            this.cbBarcelona.Size = new System.Drawing.Size(74, 17);
            this.cbBarcelona.TabIndex = 5;
            this.cbBarcelona.Text = "Barcelona";
            this.cbBarcelona.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(12, 165);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.button1_Click);
            // 
            // Export
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(131, 214);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.cbBarcelona);
            this.Controls.Add(this.cbChelsea);
            this.Controls.Add(this.cbReal);
            this.Controls.Add(this.cbArsenal);
            this.Controls.Add(this.cbPorto);
            this.Controls.Add(this.cbNone);
            this.Name = "Export";
            this.Text = "Export";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox cbNone;
        private System.Windows.Forms.CheckBox cbPorto;
        private System.Windows.Forms.CheckBox cbArsenal;
        private System.Windows.Forms.CheckBox cbReal;
        private System.Windows.Forms.CheckBox cbChelsea;
        private System.Windows.Forms.CheckBox cbBarcelona;
        private System.Windows.Forms.Button btnOK;
    }
}