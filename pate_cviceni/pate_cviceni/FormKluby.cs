﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pate_cviceni
{
    public partial class FormKluby : Form
    {
        
        public FormKluby(string[] kluby, int gol)
        {
            InitializeComponent();
            goly.Text = gol.ToString();
            for(int i = 0; i < kluby.Length; i++)
            {
                textAreaKlub.Items.Add(kluby[i].ToString());

            }
        }

        private void ok_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
