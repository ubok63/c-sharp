﻿namespace pate_cviceni
{
    partial class Form1
    {
        /// <summary>
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód generovaný Návrhářem Windows Form

        /// <summary>
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.pridej = new System.Windows.Forms.Button();
            this.vymaz = new System.Windows.Forms.Button();
            this.upravit = new System.Windows.Forms.Button();
            this.nejlepsi = new System.Windows.Forms.Button();
            this.registrovat = new System.Windows.Forms.Button();
            this.zrusit = new System.Windows.Forms.Button();
            this.ukoncit = new System.Windows.Forms.Button();
            this.listBoxHraci = new System.Windows.Forms.ListBox();
            this.Export = new System.Windows.Forms.Button();
            this.Import = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(12, 302);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(424, 134);
            this.listBox1.TabIndex = 1;
            // 
            // pridej
            // 
            this.pridej.Location = new System.Drawing.Point(365, 13);
            this.pridej.Name = "pridej";
            this.pridej.Size = new System.Drawing.Size(75, 23);
            this.pridej.TabIndex = 2;
            this.pridej.Tag = "pridej";
            this.pridej.Text = "Přidej";
            this.pridej.UseVisualStyleBackColor = true;
            this.pridej.Click += new System.EventHandler(this.pridej_Click);
            // 
            // vymaz
            // 
            this.vymaz.Location = new System.Drawing.Point(365, 43);
            this.vymaz.Name = "vymaz";
            this.vymaz.Size = new System.Drawing.Size(75, 23);
            this.vymaz.TabIndex = 3;
            this.vymaz.Tag = "vymaz";
            this.vymaz.Text = "Vymaž";
            this.vymaz.UseVisualStyleBackColor = true;
            this.vymaz.Click += new System.EventHandler(this.vymaz_Click);
            // 
            // upravit
            // 
            this.upravit.Location = new System.Drawing.Point(365, 73);
            this.upravit.Name = "upravit";
            this.upravit.Size = new System.Drawing.Size(75, 23);
            this.upravit.TabIndex = 4;
            this.upravit.Tag = "upravit";
            this.upravit.Text = "Upravit";
            this.upravit.UseVisualStyleBackColor = true;
            this.upravit.Click += new System.EventHandler(this.upravit_Click);
            // 
            // nejlepsi
            // 
            this.nejlepsi.Location = new System.Drawing.Point(365, 103);
            this.nejlepsi.Name = "nejlepsi";
            this.nejlepsi.Size = new System.Drawing.Size(75, 23);
            this.nejlepsi.TabIndex = 5;
            this.nejlepsi.Tag = "nejlepsi";
            this.nejlepsi.Text = "Nejlepší";
            this.nejlepsi.UseVisualStyleBackColor = true;
            this.nejlepsi.Click += new System.EventHandler(this.nejlepsi_Click);
            // 
            // registrovat
            // 
            this.registrovat.Location = new System.Drawing.Point(365, 133);
            this.registrovat.Name = "registrovat";
            this.registrovat.Size = new System.Drawing.Size(75, 23);
            this.registrovat.TabIndex = 6;
            this.registrovat.Tag = "registrovat";
            this.registrovat.Text = "Registrovat";
            this.registrovat.UseVisualStyleBackColor = true;
            this.registrovat.Click += new System.EventHandler(this.registrovat_Click);
            // 
            // zrusit
            // 
            this.zrusit.Location = new System.Drawing.Point(365, 163);
            this.zrusit.Name = "zrusit";
            this.zrusit.Size = new System.Drawing.Size(75, 23);
            this.zrusit.TabIndex = 7;
            this.zrusit.Tag = "zrusit";
            this.zrusit.Text = "Zrušit";
            this.zrusit.UseVisualStyleBackColor = true;
            this.zrusit.Click += new System.EventHandler(this.zrusit_Click);
            // 
            // ukoncit
            // 
            this.ukoncit.Location = new System.Drawing.Point(365, 193);
            this.ukoncit.Name = "ukoncit";
            this.ukoncit.Size = new System.Drawing.Size(75, 23);
            this.ukoncit.TabIndex = 8;
            this.ukoncit.Tag = "ukoncit";
            this.ukoncit.Text = "Ukončit";
            this.ukoncit.UseVisualStyleBackColor = true;
            this.ukoncit.Click += new System.EventHandler(this.ukoncit_Click);
            // 
            // listBoxHraci
            // 
            this.listBoxHraci.FormattingEnabled = true;
            this.listBoxHraci.Location = new System.Drawing.Point(13, 13);
            this.listBoxHraci.Name = "listBoxHraci";
            this.listBoxHraci.Size = new System.Drawing.Size(346, 264);
            this.listBoxHraci.TabIndex = 9;
            // 
            // Export
            // 
            this.Export.Location = new System.Drawing.Point(365, 223);
            this.Export.Name = "Export";
            this.Export.Size = new System.Drawing.Size(75, 23);
            this.Export.TabIndex = 10;
            this.Export.Text = "Export";
            this.Export.UseVisualStyleBackColor = true;
            this.Export.Click += new System.EventHandler(this.Export_Click);
            // 
            // Import
            // 
            this.Import.Location = new System.Drawing.Point(365, 253);
            this.Import.Name = "Import";
            this.Import.Size = new System.Drawing.Size(75, 23);
            this.Import.TabIndex = 11;
            this.Import.Text = "Import";
            this.Import.UseVisualStyleBackColor = true;
            this.Import.Click += new System.EventHandler(this.Import_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(451, 449);
            this.Controls.Add(this.Import);
            this.Controls.Add(this.Export);
            this.Controls.Add(this.listBoxHraci);
            this.Controls.Add(this.ukoncit);
            this.Controls.Add(this.zrusit);
            this.Controls.Add(this.registrovat);
            this.Controls.Add(this.nejlepsi);
            this.Controls.Add(this.upravit);
            this.Controls.Add(this.vymaz);
            this.Controls.Add(this.pridej);
            this.Controls.Add(this.listBox1);
            this.Name = "Form1";
            this.Text = "Liga Mistrů";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button pridej;
        private System.Windows.Forms.Button vymaz;
        private System.Windows.Forms.Button upravit;
        private System.Windows.Forms.Button nejlepsi;
        private System.Windows.Forms.Button registrovat;
        private System.Windows.Forms.Button zrusit;
        private System.Windows.Forms.Button ukoncit;
        private System.Windows.Forms.ListBox listBoxHraci;
        private System.Windows.Forms.Button Export;
        private System.Windows.Forms.Button Import;
    }
}

