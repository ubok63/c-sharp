﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static pate_cviceni.Hraci;

namespace pate_cviceni
{
    public partial class Form1 : Form
    {

        private PocetHracuChangeEventHandler handler;
        public static Hraci hraci = new Hraci();
        public string[] kluby;
        int pocetGolu;
        public Form1()
        {
            InitializeComponent();
            handler = delegate () {
                listBox1.Items.Add("Proběhla změna počtu nebo informace o hráčích");
                listBoxHraci.Items.Clear();
                foreach (Hrac h in hraci.seznam)
                {
                    if (h != null)
                    {
                        listBoxHraci.Items.Add(h);
                    }

                }
                hraci.seznam.Reset();
            };

        }

        
        

        private void pridej_Click(object sender, EventArgs e)
        {
            FormHrac fh = new FormHrac(null);
            fh.ShowDialog();
            if(fh.DialogResult == DialogResult.OK)
            {
                hraci.Pridej(fh.Hrac);
            }
        }

        private void vymaz_Click(object sender, EventArgs e)
        {
            if(listBoxHraci.SelectedItem != null)
            {
                hraci.Vymaz((Hrac)listBoxHraci.SelectedItem);
            }
            
        }

        private void upravit_Click(object sender, EventArgs e)
        {
            if(listBoxHraci.SelectedItem != null)
            {
                FormHrac fh = new FormHrac((Hrac)listBoxHraci.SelectedItem);
                fh.ShowDialog();
                if (fh.DialogResult == DialogResult.OK)
                {
                    hraci.Vymaz((Hrac)listBoxHraci.SelectedItem);
                    hraci.Pridej(fh.Hrac);
                }
            }
            
        }

        private void nejlepsi_Click(object sender, EventArgs e)
        {
            
            
        hraci.NajdiNejlepsiKluby(out kluby, out pocetGolu);
        FormKluby fk = new FormKluby(kluby, pocetGolu);
            fk.ShowDialog();
        }

        private void registrovat_Click(object sender, EventArgs e)
        {
            hraci.PocetZmenen += handler;
            listBox1.Items.Add("zaregistrovn handler");
        }

        private void zrusit_Click(object sender, EventArgs e)
        {
            hraci.PocetZmenen -= handler;
            listBox1.Items.Add("zrušena registrace handleru");
        }

        private void ukoncit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Export_Click(object sender, EventArgs e)
        {
            Export ex = new Export(hraci);
            ex.ShowDialog();
        }

        private void Import_Click(object sender, EventArgs e)
        {
            string jmeno = string.Empty;
            string klub = string.Empty;
            string pocetG = string.Empty;
            var fileContent = string.Empty;
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    var fileStream = openFileDialog.OpenFile();

                    using (StreamReader reader = new StreamReader(fileStream))
                    {
                        while(!reader.EndOfStream)
                        {
                            jmeno = reader.ReadLine();
                            klub = reader.ReadLine();
                            pocetG = reader.ReadLine();
                            Hrac h = new Hrac(jmeno, FotbalovyKlubInfo.DejTyp(klub), Int32.Parse(pocetG));
                            hraci.Pridej(h);
                        }
                         
                    }
                }
            }
        }
    }
}
