﻿using System;
namespace Fei
{
    namespace BaseLib
    {
        public class Reading
        {
            /// <summary>
            /// vypíše hlášku a načte double
            /// </summary>
            /// <param name="s">hláška k vypsání</param>
            /// <returns>double nebo vyhodí exception</returns>
            public static double ReadDouble(string s)
            {
                Console.WriteLine(s + ": ");
                string text = Console.ReadLine();
                double d;
                if (Double.TryParse(text, out d))
                {
                    return d;
                }
                else
                {
                    throw new Exception();
                }
            }

            /// <summary>
            /// vypíše hlášku a načte int
            /// </summary>
            /// <param name="s">hláška k vypsání</param>
            /// <returns>int nebo vyhodí exception</returns>
            public static int ReadInt(string s)
            {
                Console.WriteLine(s + ": ");
                string text = Console.ReadLine();
                int i;
                if (int.TryParse(text, out i))
                {
                    return i;
                }
                else
                {
                    throw new Exception();
                }
            }

            /// <summary>
            /// vypíše hlášku a načte string
            /// </summary>
            /// <param name="s">hláška k vypsání</param>
            /// <returns>string</returns>
            public static string ReadString(string s)
            {
                Console.WriteLine(s + ": ");
                return Console.ReadLine();
            }

            /// <summary>
            /// vypíše hlášku a načte char
            /// </summary>
            /// <param name="s">hláška k vypsání</param>
            /// <returns>char</returns>
            public static char ReadChar(string s)
            {
                Console.WriteLine(s + ": ");
                return Console.ReadKey().KeyChar;
            }
        }

        public class ExtraMath
        {
            /// <summary>
            /// metoda pro řešení kvadratické rovnice
            /// </summary>
            /// <param name="a"></param>
            /// <param name="b"></param>
            /// <param name="c"></param>
            /// <returns>null pro žádné řešení, jinak pole s kořeny</returns>
            public static double[] QuadraticEquation(double a, double b, double c)
            {
                double[] result = new double[2];
                double x1;
                double x2;
                double dis = Math.Pow(b, 2) - (4 * a * c);
                if (dis < 0)
                {
                    return null;
                }
                else if (dis == 0)
                {
                    x1 = (-b) / (2 * a);
                    result[0] = x1;
                    return result;
                }
                else
                {
                    x1 = (-b + Math.Sqrt(dis)) / (2 * a);
                    x2 = (-b + Math.Sqrt(dis)) / (2 * a);
                    result[0] = x1;
                    result[1] = x2;
                    return result;
                }
            }

            /// <summary>
            /// vygeneruje náhodné číslo v rozsahu
            /// </summary>
            /// <param name="r"></param>
            /// <param name="min"></param>
            /// <param name="max"></param>
            /// <returns>double</returns>
            public static double Rng(Random r, int min, int max)
            {
                return r.Next(min, max);
            }
        }

        public class MathConvertor
        {
            /// <summary>
            /// konvertor decimal na binár
            /// </summary>
            /// <param name="x"></param>
            /// <returns>string</returns>
            public static string DecToBin(double x)
            {
                string s = "";
                for (int i = 0; x > 0; i++)
                {
                    s += x % 2;
                    x = x / 2;
                }
                return s;
            }

            /// <summary>
            /// konvertor binár na decimál
            /// </summary>
            /// <param name="x"></param>
            /// <returns>double</returns>
            public static double BinToDec(string x)
            {
                return Convert.ToInt32(x, 2);
            }

            /// <summary>
            /// konvertor decimál na román
            /// </summary>
            /// <param name="number"></param>
            /// <returns>string</returns>
            public static string DecToRoman(double number)
            {
                if ((number < 0) || (number > 3999)) throw new ArgumentOutOfRangeException("insert value betwheen 1 and 3999");
                if (number < 1) return string.Empty;
                if (number >= 1000) return "M" + DecToRoman(number - 1000);
                if (number >= 900) return "CM" + DecToRoman(number - 900);
                if (number >= 500) return "D" + DecToRoman(number - 500);
                if (number >= 400) return "CD" + DecToRoman(number - 400);
                if (number >= 100) return "C" + DecToRoman(number - 100);
                if (number >= 90) return "XC" + DecToRoman(number - 90);
                if (number >= 50) return "L" + DecToRoman(number - 50);
                if (number >= 40) return "XL" + DecToRoman(number - 40);
                if (number >= 10) return "X" + DecToRoman(number - 10);
                if (number >= 9) return "IX" + DecToRoman(number - 9);
                if (number >= 5) return "V" + DecToRoman(number - 5);
                if (number >= 4) return "IV" + DecToRoman(number - 4);
                if (number >= 1) return "I" + DecToRoman(number - 1);
                throw new ArgumentOutOfRangeException("something bad happened");
            }

            /// <summary>
            /// konvertor román na decimál
            /// </summary>
            /// <param name="number"></param>
            /// <returns>double</returns>
            public static double RomanToDec(string number)
            {
                number = number.ToUpper();
                var result = 0;

                foreach (var letter in number)
                {
                    result += ConvertLetterToNumber(letter);
                }

                if (number.Contains("IV") || number.Contains("IX"))
                    result -= 2;

                if (number.Contains("XL") || number.Contains("XC"))
                    result -= 20;

                if (number.Contains("CD") || number.Contains("CM"))
                    result -= 200;


                return result;
            }

            /// <summary>
            /// pomocná třída, pímeno na číslo
            /// </summary>
            /// <param name="letter"></param>
            /// <returns>int</returns>
            private static int ConvertLetterToNumber(char letter)
            {
                switch (letter)
                {
                    case 'M':
                        {
                            return 1000;
                        }

                    case 'D':
                        {
                            return 500;
                        }

                    case 'C':
                        {
                            return 100;
                        }

                    case 'L':
                        {
                            return 50;
                        }

                    case 'X':
                        {
                            return 10;
                        }

                    case 'V':
                        {
                            return 5;
                        }

                    case 'I':
                        {
                            return 1;
                        }

                    default:
                        {
                            throw new ArgumentException("Ivalid charakter");
                        }
                }
            }
        }
    }
}