﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PacManLubosDvorak
{
    public class Map
    {
        private const int PICTURE_SIZE = 40;
        public string[,] ArrayMap { get; set; }

        public Map()
        {
            ArrayMap = new string[20, 11];
            LoadFile();
        }

        /// <summary>
        /// load map to string from file
        /// </summary>
        public void LoadFile()
        {
            try
            {
                using (StreamReader sr = new StreamReader("../../map/pac_man_map.txt"))
                {
                    
                    string all = sr.ReadToEnd();
                    CreateMap(all);
                }
            }
            catch(Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// parse string to array
        /// </summary>
        /// <param name="all">map string</param>
        public void CreateMap(string all)
        {
            int i = 0;
            int j = 0;
            all = all.Replace("\n", "");
            all = all.Replace("\r", "");
            foreach (char s in all)
            {
                if (i == ArrayMap.GetLength(0))
                {
                    i = 0;
                    j++;
                }
                ArrayMap[i, j] = s.ToString();
                i++;
            }
        }

        /// <summary>
        /// load string from srray to string for saving game
        /// </summary>
        /// <param name="array">map array</param>
        /// <returns>parsed string</returns>
        public static string SaveGame(string[,] array)
        {
            string s = string.Empty;
            for(int i = 0; i < array.GetLength(0); i++)
            {
                for(int j = 0; j < array.GetLength(1); j++)
                {
                    s += array[i, j];
                }
                s += "\n";
            }
            return s;
        }

        /// <summary>
        /// saves map to array
        /// </summary>
        /// <returns>array with map</returns>
        public string[,] SaveMapToArray(TableLayoutPanel mapGrid, Pacman pac)
        {
            string[,] s = new string[11, 20];
            for (int i = 0; i < mapGrid.RowCount; i++)
            {
                for (int j = 0; j < mapGrid.ColumnCount; j++)
                {
                    Control c = mapGrid.GetControlFromPosition(j, i);
                    if (c == null)
                    {
                        s[i, j] = "N";
                    }
                    else if (c.GetType() == typeof(Panel))
                    {
                        s[i, j] = "1";
                    }
                    else if (c.GetType() == typeof(CoinPictureBox))
                    {
                        s[i, j] = "0";
                    }
                    else if (c.GetType() == typeof(BonusPictureBox))
                    {
                        s[i, j] = "B";
                    }
                }
            }
            s[pac.Player.Location.Y / PICTURE_SIZE, pac.Player.Location.X / PICTURE_SIZE] = "P";
            return s;
        }

        /// <summary>
        /// adds game properties like enemies, points, bonus time to save string
        /// </summary>
        /// <param name="s">array with map</param>
        /// <returns>save string</returns>
        public string AddGamePropertiesToSaveString(string[,] s, int bonusTime, int respawnTime, ArrayList ghosts, Pacman pac)
        {
            string saveString = Map.SaveGame(s);

            saveString += "+\n";
            saveString += bonusTime + ";\n";
            saveString += respawnTime + ";\n";
            saveString += pac.Points + ";";
            ghosts.Capacity = ghosts.Count;
            foreach (Ghost g in ghosts)
            {
                saveString += "\n" + g.Enemy.Location.X + "," + g.Enemy.Location.Y + "*";
            }
            saveString = saveString.Remove(saveString.Length - 1);
            return saveString;
        }
    }
}
