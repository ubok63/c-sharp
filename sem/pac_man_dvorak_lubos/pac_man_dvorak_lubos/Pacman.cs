﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

public enum Direction
{
    Left = 1,
    Right = 2,
    Up = 3,
    Down = 4
}

namespace PacManLubosDvorak
{

    public class Pacman
    {
        private const int PICTURE_SIZE = 40;
        public int Points { get; set;}
        public Direction Direction { get; set; }
        public Direction NextDirection { get; set; }
        public event EventHandler<Control> Bonus;
        public event EventHandler<Control> Coin;
        public event EventHandler Move;


        private static Image[] images = new Image[4];

        public PictureBox Player {get; set;}

        public Pacman()
        {
            Points = 0;
            Direction = Direction.Right;
            NextDirection = Direction.Right;
            Player = new PictureBox();
            Player.Height = PICTURE_SIZE;
            Player.Width = PICTURE_SIZE;
            Player.Margin = new Padding(0);
            Player.Image = images[0];
        }
        static Pacman()
        {
            LoadImages();
        }

        /// <summary>
        /// load pacman images from file
        /// </summary>
        private static void LoadImages()
        {
            FileStream fs = new FileStream("../../imgs/pacmanRight.jpg", FileMode.Open, FileAccess.Read);
            images[0] = Image.FromStream(fs);
            fs = new FileStream("../../imgs/pacmanLeft.jpg", FileMode.Open, FileAccess.Read);
            images[1] = Image.FromStream(fs);
            fs = new FileStream("../../imgs/pacmanUp.jpg", FileMode.Open, FileAccess.Read);
            images[2] = Image.FromStream(fs);
            fs = new FileStream("../../imgs/pacmanDown.jpg", FileMode.Open, FileAccess.Read);
            images[3] = Image.FromStream(fs);
            fs.Close();
        }

        /// <summary>
        /// invokes event when pacman eats bonus
        /// </summary>
        /// <param name="c"></param>
        public virtual void OnBonusEaten(Control c)
        {
            Bonus?.Invoke(this, c);
        }

        /// <summary>
        /// invokes event when pacman eats coin
        /// </summary>
        /// <param name="c"></param>
        public virtual void OnCoinEaten(Control c)
        {
            Coin?.Invoke(this, c);
        }

        /// <summary>
        /// every time tick pacman will move
        /// </summary>
        public virtual void onTimeTick()
        {
            Move?.Invoke(this, new EventArgs());
        }

        /// <summary>
        /// changes next direction
        /// </summary>
        /// <param name="d"></param>
        public void ChangeNextDirection(Direction d)
        {
            NextDirection = d;
        }

        /// <summary>
        /// changes direction and picture based on direction of movement
        /// </summary>
        /// <param name="d"></param>
        public void ChangeDirection(Direction d)
        {
            switch (d)
            {
                case Direction.Right:
                    Player.Image = images[0];
                    break;
                case Direction.Left:
                    Player.Image = images[1];
                    break;
                case Direction.Up:
                    Player.Image = images[2];
                    break;
                case Direction.Down:
                    Player.Image = images[3];
                    break;

            }
            Direction = d;
        }

    }
}
