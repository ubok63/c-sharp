﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PacManLubosDvorak
{
    public partial class form : System.Windows.Forms.Form
    {
        private const int PICTURE_SIZE = 40;
        private const int PIXELS_MOVEMENT = 2;
        private const int RESPAWN_TIME = 10000;
        private const int BONUS_TIME = 10000;
        private const int GHOST_RESPAWN_X = 320;
        private const int GHOST_RESPAWN_Y = 200;
        private Pacman pac;
        private Map map;
        private ArrayList ghosts = new ArrayList();
        private int coinCount;
        private bool paused = true;
        private bool gameStart = false;
        private Coin coin;
        private int bonusTime = 0;
        private int respawnTime = 0;
        private Label label;
        public form()
        {
            InitializeComponent();
            timerPacman.Tick += PacMoveHandler;
            timerGhost.Tick += GhostMoveHandler;
            timerBonus.Tick += BonusEatenHandler;
            timerRespawn.Tick += RespawnHandler;
            WelcomeLabel();
            panelPac.Controls.Add(label);
            label.BringToFront();
        }

        /// <summary>
        /// creates welcome label
        /// </summary>
        private void WelcomeLabel()
        {
            label = new Label();
            label.Text = "WELCOME TO PACMAN";
            label.ForeColor = Color.White;
            label.BackColor = Color.Black;
            label.Font = new Font("Arial", 64, FontStyle.Bold);
            label.Size = new Size(800, 440);
            label.AutoSize = false;
            label.TextAlign = ContentAlignment.MiddleCenter;
            label.Dock = DockStyle.Fill;
        }

        /// <summary>
        /// methid will clear table layout panel and print new map based on array
        /// </summary>
        private void PrintMap()
        {
            panelPac.Controls.Remove(label);
            ClearGhosts();
            if(pac != null)
                panelPac.Controls.Remove(pac.Player);
            ghosts.Clear();
            coinCount = 0;
            mapGrid.Controls.Clear();
            for(int i = 0; i < map.ArrayMap.GetLength(0); i++)
            {
                for(int j = 0; j < map.ArrayMap.GetLength(1); j++)
                {
                    switch (map.ArrayMap[i, j])
                    {
                        case "0":
                            InsertCoin(i, j);
                            break;
                        case "1":
                            InsertWall(i, j);
                            break;
                        case "P":
                            InsertPacman(i, j);
                            break;
                        case "G":
                            InsertGhost(i, j);
                            break;
                        case "B":
                            InsertBonus(i, j);
                            break;
                        case "N":
                            break;
                    }
                }
            }
            pac.Points = 0;
        }

        /// <summary>
        /// insert coin to field on map
        /// </summary>
        /// <param name="i"> column of field</param>
        /// <param name="j"> row of field</param>
        private void InsertCoin(int i, int j)
        {
            coin = new Coin();
            coin.PacCoin.Margin = new Padding(0);
            mapGrid.Controls.Add(coin.PacCoin, i, j);
            coinCount++;
        }

        /// <summary>
        /// insert wall to field on map
        /// </summary>
        /// <param name="i"> column of field</param>
        /// <param name="j"> row of field</param>
        private void InsertWall(int i, int j)
        {
            Panel p;
            p = new Panel();
            p.BackColor = Color.Navy;
            p.Margin = new Padding(0);
            mapGrid.Controls.Add(p, i, j);
        }

        /// <summary>
        /// insert pacman to field on map and calculate coords
        /// </summary>
        /// <param name="i"> column of field</param>
        /// <param name="j"> row of field</param>
        private void InsertPacman(int i, int j)
        {
            Panel p;
            Control c;
            pac = new Pacman();
            AddEventsToPacman(pac);
            panelPac.Controls.Add(pac.Player);
            p = new Panel();
            p.Margin = new Padding(0);
            mapGrid.Controls.Add(p, i, j);
            c = mapGrid.GetControlFromPosition(i, j);
            mapGrid.Controls.Remove(mapGrid.GetControlFromPosition(i, j));
            pac.Player.BringToFront();
            c.Margin = new Padding(0);
            pac.Player.Location = new Point(c.Location.X, c.Location.Y);
        }

        /// <summary>
        /// insert bonus to field on map
        /// </summary>
        /// <param name="i"> column of field</param>
        /// <param name="j"> row of field</param>
        private void InsertBonus(int i, int j)
        {
            Bonus bonus = new Bonus();
            bonus.Cherry.Margin = new Padding(0);
            mapGrid.Controls.Add(bonus.Cherry, i, j);
        }

        /// <summary>
        /// insert ghost to field on map and calculate coords
        /// </summary>
        /// <param name="i"> column of field</param>
        /// <param name="j"> row of field</param>
        private void InsertGhost(int i, int j)
        {
            Panel p;
            Control c;
            Ghost ghost = new Ghost();
            AddEventsToGhost(ghost);
            ghosts.Add(ghost);
            panelPac.Controls.Add(ghost.Enemy);
            p = new Panel();
            p.Margin = new Padding(0);
            mapGrid.Controls.Add(p, i, j);
            c = mapGrid.GetControlFromPosition(i, j);
            mapGrid.Controls.Remove(mapGrid.GetControlFromPosition(i, j));
            ghost.Enemy.BringToFront();
            c.Margin = new Padding(0);
            ghost.Enemy.Location = new Point(c.Location.X, c.Location.Y);
        }

        /// <summary>
        /// adding custom events to ghost
        /// </summary>
        /// <param name="g">ghost</param>
        public void AddEventsToGhost(Ghost g)
        {
            g.Kill += GhostKillingPlayerEventHandler;
            g.Die += GhostIsEatenEventHandler;
        }

        /// <summary>
        /// adding custom events to pacman
        /// </summary>
        /// <param name="p"></param>
        public void AddEventsToPacman(Pacman p)
        {
            pac.Bonus += PacEatBonusEventHandler;
            pac.Coin += PacEatCoinEventHandler;
            pac.Move += PacMovementEventHandler;
        }

        /// <summary>
        /// invoked by clicking on New Game menu item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!gameStart)
            {
                map = new Map();
                PrintMap();
            }
        }

        /// <summary>
        /// clears ghosts from panel behind table layout panel
        /// </summary>
        private void ClearGhosts()
        {
            foreach(Ghost g in ghosts)
            {
                panelPac.Controls.Remove(g.Enemy);
            }
        }

        /// <summary>
        /// fire event when certain key is pressed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void form_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Space)
            {
                gameStart = true;
                paused = false;
                StartGame();
            }
            if (e.KeyCode == Keys.Up || e.KeyCode == Keys.W)
            {
                 pac.ChangeNextDirection(Direction.Up);
            }
            if (e.KeyCode == Keys.Down || e.KeyCode == Keys.S)
            {
                 pac.ChangeNextDirection(Direction.Down);
            }
            if (e.KeyCode == Keys.Left || e.KeyCode == Keys.A)
            {
                 pac.ChangeNextDirection(Direction.Left);
            }
            if (e.KeyCode == Keys.Right || e.KeyCode == Keys.D)
            {
                 pac.ChangeNextDirection(Direction.Right);
            }
            if(e.KeyCode == Keys.F1)
            {
                pauseToolStripMenuItem_Click(Keys.F1, new EventArgs());
            }
            if(e.KeyCode == Keys.F2)
            {
                resumeToolStripMenuItem_Click(Keys.F2, new EventArgs());
            }
            if(e.KeyCode == Keys.F3)
            {
                saveToolStripMenuItem_Click(Keys.F3, new EventArgs());
            }
        }

        /// <summary>
        /// checks what field is in front of pacman or ghost (points in both corners of direction of move) (1px in front of it)
        /// </summary>
        /// <param name="d">direction of movement</param>
        /// <param name="g">for pacman: null, for ghost: ghost which is moving</param>
        /// <returns>return CheckControl number of what is in front of moving object</returns>
        private int CheckNextMove(Direction d, Ghost g)
        {
            Control c1 = null;
            Control c2 = null;
            switch (d)
            {
                case Direction.Up:
                    if(g != null)
                    {
                        c1 = mapGrid.GetChildAtPoint(new Point(g.Enemy.Location.X, g.Enemy.Location.Y - 1));
                        c2 = mapGrid.GetChildAtPoint(new Point(g.Enemy.Location.X + PICTURE_SIZE - 1, g.Enemy.Location.Y - 1));
                    }
                    else
                    {
                        c1 = mapGrid.GetChildAtPoint(new Point(pac.Player.Location.X, pac.Player.Location.Y - 1));
                        c2 = mapGrid.GetChildAtPoint(new Point(pac.Player.Location.X + PICTURE_SIZE - 1, pac.Player.Location.Y - 1));
                    }
                    break;
                case Direction.Down:
                    if(g != null)
                    {
                        c1 = mapGrid.GetChildAtPoint(new Point(g.Enemy.Location.X, g.Enemy.Location.Y + PICTURE_SIZE));
                        c2 = mapGrid.GetChildAtPoint(new Point(g.Enemy.Location.X + PICTURE_SIZE - 1, g.Enemy.Location.Y + PICTURE_SIZE));
                    }
                    else
                    {
                        c1 = mapGrid.GetChildAtPoint(new Point(pac.Player.Location.X, pac.Player.Location.Y + PICTURE_SIZE));
                        c2 = mapGrid.GetChildAtPoint(new Point(pac.Player.Location.X + PICTURE_SIZE - 1, pac.Player.Location.Y + PICTURE_SIZE));
                    }
                    break;
                case Direction.Left:
                    if(g != null)
                    {
                        c1 = mapGrid.GetChildAtPoint(new Point(g.Enemy.Location.X - 1, g.Enemy.Location.Y));
                        c2 = mapGrid.GetChildAtPoint(new Point(g.Enemy.Location.X - 1, g.Enemy.Location.Y + PICTURE_SIZE - 1));
                    }
                    else
                    {
                        c1 = mapGrid.GetChildAtPoint(new Point(pac.Player.Location.X - 1, pac.Player.Location.Y));
                        c2 = mapGrid.GetChildAtPoint(new Point(pac.Player.Location.X - 1, pac.Player.Location.Y + PICTURE_SIZE - 1));
                    }
                    break;
                case Direction.Right:
                    if(g != null)
                    {
                        c1 = mapGrid.GetChildAtPoint(new Point(g.Enemy.Location.X + PICTURE_SIZE, g.Enemy.Location.Y));
                        c2 = mapGrid.GetChildAtPoint(new Point(g.Enemy.Location.X + PICTURE_SIZE, g.Enemy.Location.Y + PICTURE_SIZE - 1));
                    }
                    else
                    {
                        c1 = mapGrid.GetChildAtPoint(new Point(pac.Player.Location.X + PICTURE_SIZE, pac.Player.Location.Y));
                        c2 = mapGrid.GetChildAtPoint(new Point(pac.Player.Location.X + PICTURE_SIZE, pac.Player.Location.Y + PICTURE_SIZE - 1));
                    }
                    break;
            }
            if (g != null)
                return CheckControl(c1, c2, true);
            else
                return CheckControl(c1, c2, false);
        }

        /// <summary>
        /// checks if at least one panel is in front of moving object so it can't move. 
        /// </summary>
        /// <param name="c1">control in front of first corner in direction of movement</param>
        /// <param name="c2">control in front of second corner in direction of movement</param>
        /// <param name="ghost">false for pacman, true for ghost</param>
        /// <returns>
        /// 0 if next field is empty
        /// 1 if at least one field in front of moving object is panel (wall) - forbids movement
        /// 2 if next field is coin
        /// 3 if next field is bonus
        /// 4 for testing purpose, should not occur
        /// </returns>
        private int CheckControl(Control c1, Control c2, bool ghost)
        {
            if (c1 == null && c2 == null)
            {
                return 0;
            }
            else if ((c1 != null && c1.GetType() == typeof(Panel)) || (c2 != null && c2.GetType() == typeof(Panel)) ||
                ((c1 != null && c1.GetType() == typeof(Panel)) && (c2 != null && c2.GetType() == typeof(Panel))))
            {
                return 1;
            }
            else if ((c1.GetType() == typeof(CoinPictureBox) && c2.GetType() == typeof(CoinPictureBox)))
            {
                if (!ghost)
                {
                    pac.OnCoinEaten(c1);
                }
                return 2;
            }
            else if (c1.GetType() == typeof(BonusPictureBox) && c2.GetType() == typeof(BonusPictureBox))
            {
                if (!ghost)
                {
                    pac.OnBonusEaten(c1);
                }
                return 3;
            }
            else
            {
                return 4;
            }
        }

        /// <summary>
        /// removes coin, adds point to pacman, if its last con, player is winner
        /// </summary>
        /// <param name="c"></param>
        private void PacEatCoinEventHandler(object sender, Control c)
        {
            mapGrid.Controls.Remove(c);
            pac.Points += 1;
            score.Text = "Points: " + pac.Points;
            if (pac.Points == coinCount)
            {
                gameStart = false;
                timerPacman.Enabled = false;
                timerBonus.Enabled = false;
                timerGhost.Enabled = false;
                DialogResult res = MessageBox.Show("You have won", "Winner", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (res == DialogResult.OK)
                {
                    ClearGhosts();
                    panelPac.Controls.Remove(pac.Player);
                    mapGrid.Controls.Clear();
                    score.Text = "Points: 0";
                    bonusLabel.Text = "Bonus: 0s";
                    panelPac.Controls.Add(label);
                    label.BringToFront();
                }
            }
        }

        /// <summary>
        /// removes bonus, activate bonus for player
        /// </summary>
        /// <param name="c"></param>
        private void PacEatBonusEventHandler(object sender, Control c)
        {
            foreach (Ghost g in ghosts)
            {
                g.GhostIsEatable();
            }
            mapGrid.Controls.Remove(c);
            bonusTime = BONUS_TIME;
            bonusLabel.Text = "Bonus: 10s";
            timerBonus.Enabled = true;
        }

        /// <summary>
        /// fire event when pause menu item was clicked
        /// pausing all timers
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pauseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (paused == false && gameStart == true)
            {
                timerPacman.Stop();
                timerGhost.Stop();
                timerBonus.Enabled = false;
                timerRespawn.Stop();
                paused = true;
            }
        }

        /// <summary>
        /// fires event when resume menu item was clicked
        /// resuming all timers which were enabled
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void resumeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(paused == true && gameStart == true)
            {
                timerPacman.Start();
                timerGhost.Start();
                paused = false;
                if(respawnTime > 0)
                {
                    timerRespawn.Enabled = true;
                }
                if (bonusTime > 0)
                {
                    timerBonus.Enabled = true;
                }
            }
        }

        /// <summary>
        /// triggers when ghost was eaten
        /// spawns new ghost after 10s
        /// </summary>
        /// <param name="x">x coords of spawning location</param>
        /// <param name="y">y coords of spawning location</param>
        private void GhostRespawn(int x, int y)
        {
            Ghost ghost = new Ghost();
            AddEventsToGhost(ghost);
            ghosts.Add(ghost);
            panelPac.Controls.Add(ghost.Enemy);
            ghost.Enemy.BringToFront();
            ghost.Enemy.Location = new Point(x, y);
        }

        /// <summary>
        /// adds delegate to pacman timer
        /// every tick will check if pacman can move, if yes, pacman will move by 2px
        /// </summary>
        private void PacMoveHandler(object sender, EventArgs e)
        {
            if (CheckNextMove(pac.NextDirection, null) != 1)
            {
                pac.ChangeDirection(pac.NextDirection);
            }
            pac.onTimeTick();
        }


        /// <summary>
        /// check if pacman can move, if yes, pacman will move by 2px
        /// </summary>
        /// <param name="sender">pacman</param>
        /// <param name="e"></param>
        private void PacMovementEventHandler(object sender, EventArgs e)
        {
            Point point = ((Pacman)sender).Player.Location;
            switch (((Pacman)sender).Direction)
            {
                case Direction.Up:
                    if (CheckNextMove(Direction.Up, null) != 1)
                        point.Y -= PIXELS_MOVEMENT;
                    break;
                case Direction.Down:
                    if (CheckNextMove(Direction.Down, null) != 1)
                        point.Y += PIXELS_MOVEMENT;
                    break;
                case Direction.Left:
                    if (CheckNextMove(Direction.Left, null) != 1)
                        point.X -= PIXELS_MOVEMENT;
                    break;
                case Direction.Right:
                    if (CheckNextMove(Direction.Right, null) != 1)
                        point.X += PIXELS_MOVEMENT;
                    break;
            }
            ((Pacman)sender).Player.Location = new Point(point.X, point.Y);
        }

        /// <summary>
        /// adds delegate to ghost timer
        /// every tick will check:
        ///     -if ghost intersect with pacman, if yes and ghost is not eatable - player lose game, if ghost is eatable, ghost is eaten
        ///     -if ghost is on crossroad - if yes it will change ghosts direction
        ///     -if player is nearby - id yes it will change ghosts direction
        ///     -if ghost can move - if not it will change ghosts direction
        /// then ghost will move in its direction by 2px
        /// </summary>
        private void GhostMoveHandler(object sender, EventArgs e)
        {
            List<Direction> directions;
            foreach (Ghost g in ghosts.ToArray())
            {
                directions = new List<Direction> { Direction.Up, Direction.Down, Direction.Left, Direction.Right };
                if (g.KillPlayer(pac.Player) && !g.Eatable)
                {
                    g.OnKill();
                    break;
                }
                else if (g.KillPlayer(pac.Player) && g.Eatable)
                {
                    g.OnDie();
                    continue;
                }
                List<Direction> cross = g.CheckCrossroad(mapGrid);
                if (cross != null)
                {
                    g.ChangeDirectionByArray(cross);
                }
                directions.Remove(g.OppositeDirection());
                g.Direction = g.IsPlayerNearby(pac);
                while (CheckNextMove(g.Direction, g) == 1)
                {
                    g.ChangeDirectionByArray(directions);
                }
                g.GhostMovement();
            }
        }

        /// <summary>
        /// occurs when ghost is eaten
        /// ghost is removed from game and timer for respawn is enabled
        /// </summary>
        /// <param name="sender"> ghost which triggers event</param>
        /// <param name="e"></param>
        private void GhostIsEatenEventHandler(object sender, EventArgs e)
        {
            ghosts.Remove((Ghost)sender);
            panelPac.Controls.Remove(((Ghost)sender).Enemy);
            respawnTime += RESPAWN_TIME;
            timerRespawn.Enabled = true;
        }

        /// <summary>
        /// occurs when player is eaten by ghost, game ends
        /// </summary>
        /// <param name="sender">ghost which triggers event</param>
        /// <param name="e"></param>
        private void GhostKillingPlayerEventHandler(object sender, EventArgs e)
        {
            gameStart = false;
            timerPacman.Enabled = false;
            timerBonus.Enabled = false;
            timerGhost.Enabled = false;
            timerRespawn.Enabled = false;
            DialogResult res = MessageBox.Show("Ghost have killed you. Wanna try again?", "Loser", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (res == DialogResult.Yes)
            {
                foreach (Ghost gh in ghosts)
                {
                    panelPac.Controls.Remove(gh.Enemy);
                }
                panelPac.Controls.Remove(pac.Player);
                mapGrid.Controls.Clear();
                PrintMap();
            }
            if (res == DialogResult.No)
            {
                this.Close();
            }
        }
            
        /// <summary>
        /// adds delegate to bonus timer
        /// every tick will lower bonus remaining time by 1s
        /// triggers when bonus is eaten
        /// after 10s timer is disabled and bonus ends
        /// </summary>
        private void BonusEatenHandler(object sender, EventArgs e)
        {
            bonusTime -= timerBonus.Interval;
            bonusLabel.Text = "Bonus: " + bonusTime / 1000 + "s";
            if (bonusTime == 0)
            {
                foreach (Ghost g in ghosts)
                {
                    g.Eatable = false;
                    g.GhostIsNotEatable();
                }
                timerBonus.Enabled = false;
            }
        }
           
        /// <summary>
        /// add delegate to ghost respawn timer
        /// triggers when ghost is eaten
        /// after 10s timer is disabled and ghost is spawned
        /// </summary>
        private void RespawnHandler(object sender, EventArgs e)
        {
            respawnTime -= timerRespawn.Interval;
            if (respawnTime == 0 || respawnTime % RESPAWN_TIME == 0)
            {
                GhostRespawn(GHOST_RESPAWN_X, GHOST_RESPAWN_Y);
                if (respawnTime == 0)
                {
                    timerRespawn.Enabled = false;
                }
            }
        }

        /// <summary>
        /// starts game by enabling pacman and ghost timers
        /// </summary>
        private void StartGame()
        {
            timerPacman.Enabled = true;
            timerGhost.Enabled = true;
        }

        /// <summary>
        /// fires event when save menu item is clicked
        /// if game is paused saves players progress to file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (gameStart == true && paused == true)
            {
                string[,] s = map.SaveMapToArray(mapGrid, pac);

                string saveString = map.AddGamePropertiesToSaveString(s, bonusTime, respawnTime, ghosts, pac);

                SaveFileDialog save = new SaveFileDialog();
                save.Filter = "txt files (*.txt)|*.txt";
                save.FilterIndex = 2;
                save.RestoreDirectory = true;

                if (save.ShowDialog() == DialogResult.OK)
                {
                    StreamWriter writer = new StreamWriter(save.OpenFile());
                    foreach(char c in saveString)
                    {
                        writer.Write(c);
                    }
                    writer.Dispose();
                    writer.Close();
                }
            }
        }

        /// <summary>
        /// fires event by clicking on load menu item
        /// load saved game from file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!gameStart || paused)
            {
                map = new Map();
                string loadedMap = "";
                using (OpenFileDialog openFileDialog = new OpenFileDialog())
                {
                    openFileDialog.InitialDirectory = "c:\\";
                    openFileDialog.Filter = "txt files (*.txt)|*.txt";
                    openFileDialog.FilterIndex = 2;
                    openFileDialog.RestoreDirectory = true;

                    if (openFileDialog.ShowDialog() == DialogResult.OK)
                    {
                        var fileStream = openFileDialog.OpenFile();

                        using (StreamReader reader = new StreamReader(fileStream))
                        {
                            while (!reader.EndOfStream)
                            {
                                loadedMap += reader.ReadLine();
                            }
                        }
                        LoadMap(loadedMap);
                    }
                }
            }
        }

        /// <summary>
        /// rebuild map from load string which was read from file
        /// </summary>
        /// <param name="s">load string from file</param>
        private void LoadMap(string s)
        {
            string[] splitted = s.Split(new[] { '+' });
            map.CreateMap(splitted[0]);
            PrintMap();
            splitted[1] = splitted[1].Replace("\n", "");
            splitted[1] = splitted[1].Replace("\r", "");
            string[] properties = splitted[1].Split(new[] { ';' });
            bonusTime = Int32.Parse(properties[0]);
            bonusLabel.Text = "Bonus: " + bonusTime / 1000 + "s";
            respawnTime = Int32.Parse(properties[1]);
            pac.Points = Int32.Parse(properties[2]);
            score.Text = "Score: " + pac.Points;
            coinCount += pac.Points;
            string[] enemies = properties[3].Split(new[] { '*' });
            for(int i = 0; i < enemies.Length; i++)
            {
                Ghost ghost = new Ghost();
                AddEventsToGhost(ghost);
                ghosts.Add(ghost);
                panelPac.Controls.Add(ghost.Enemy);
                ghost.Enemy.BringToFront();
                string[] location = enemies[i].Split(new[] { ',' });
                ghost.Enemy.Location = new Point(Int32.Parse(location[0]), Int32.Parse(location[1]));
            }
            if(respawnTime > 0)
            {
                timerRespawn.Enabled = true;
            }
            if(bonusTime > 0)
            {
                timerBonus.Enabled = true;
                foreach(Ghost g in ghosts)
                {
                    g.GhostIsEatable();
                }
            }
        }

        /// <summary>
        /// fires event by clicking on restart menu item
        /// restarts game
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void restartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            mapGrid.Controls.Clear();
            panelPac.Controls.Remove(pac.Player);
            ClearGhosts();
            PrintMap();
            timerGhost.Stop();
            timerPacman.Stop();
            timerRespawn.Stop();
            timerBonus.Stop();
            bonusTime = 10000;
            respawnTime = 0;
            score.Text = "Score: 0";
            bonusLabel.Text = "Bonus: 0s";
        }

        /// <summary>
        /// fires event by clicking on info menu item
        /// shows info about game
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void infoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Welcome in my little project. It is simple, yet original like Pacman. To start new game please click on New Game. " +
                "You will start the game by pressing space. You can move Pacman by pressing WASD or arrows. To win game you have to eat all coins in the map. " +
                "After eating cherry the ghost will turn blue and be eatable for 10s. Be careful, if you eat ghost new one will appear adter 10s " +
                "approximately in the middle of map. At any time you can pause the game and resume it. If game is paused you can save it and then load it " +
                "from the file where u saved it.");
        }

        private void endToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
