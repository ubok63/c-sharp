﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PacManLubosDvorak
{

    class Bonus
    {
        private const int PICTURE_SIZE = 40;
        public PictureBox Cherry { get; set; }

        private static Image image;

        public Bonus()
        {
            Cherry = new BonusPictureBox();

            Cherry.Image = image;
            Cherry.Height = PICTURE_SIZE;
            Cherry.Width = PICTURE_SIZE;
        }

        static Bonus()
        {
            LoadImage();
        }

        private static void LoadImage()
        {
            FileStream fs = new FileStream("../../imgs/bonus.png", FileMode.Open, FileAccess.Read);
            image = Image.FromStream(fs);
            fs.Close();
        }

    }
}
