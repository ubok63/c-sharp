﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PacManLubosDvorak
{
    class Coin
    {
        private const int PICTURE_SIZE = 40;
        public PictureBox PacCoin {get; set;}
        private static Image image;

        public Coin()
        {
            PacCoin = new CoinPictureBox();

            PacCoin.Image = image;
            PacCoin.Height = PICTURE_SIZE;
            PacCoin.Width = PICTURE_SIZE;
        }
        static Coin()
        {
            LoadImage();
        }

        private static void LoadImage()
        {
            FileStream fs = new FileStream("../../imgs/coin.png", FileMode.Open, FileAccess.Read);
            image = Image.FromStream(fs);
            fs.Close();
        }
    }
}
