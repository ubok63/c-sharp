﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace osme_cviceni
{
    class Element<K, V>
    {
        public K Key;
        public V Value;
        public Element<K, V> Next;

        public Element(K key, V value)
        {
            this.Key = key;
            this.Value = value;
            Next = null;
        }

    }

    class MinMaxHashTable<K, V>
    {
        public Element<K, V>[] arr;
        public K minKey = default(K);
        public K maxKey = default(K);
        public int count = 0;
        public int size;
        
        public MinMaxHashTable(int size)
        {
            this.size = size;
            arr = new Element<K, V>[size];
        }

        public MinMaxHashTable()
        {
            this.size = 20;
            arr = new Element<K, V>[20];
        }

        public IEnumerable<KeyValuePair<K, V>> this[K minimum, K maximum]
        {
            get
            {
                List<KeyValuePair<K, V>> list = new List<KeyValuePair<K, V>>();
                for (int i = 0; i < size; i++)
                {
                    Element<K, V> rootElement = arr[i];
                    while (rootElement != null)
                    {
                        if (ApprovedToInsert(rootElement.Key, minimum, maximum))
                            list.Add(new KeyValuePair<K, V>(rootElement.Key, rootElement.Value));
                        rootElement = rootElement.Next;
                    }
                }
                return list;
            }
        }



        private int GetElementIndex(K key)
        {
            int elementIndex = key.GetHashCode() % size;
            if (elementIndex < 0)
            {
                return Math.Abs(elementIndex);
            }
            return elementIndex;
        }

        public void Add(K key, V value)
        {
            int elementIndex = GetElementIndex(key);

            Element<K, V> rootElement = arr[elementIndex];
            while (rootElement != null)
            {
                if (rootElement.Key.Equals(key))
                {
                    throw new ArgumentException();
                }
                rootElement = rootElement.Next;
            }

            rootElement = arr[elementIndex];
            Element<K, V> element = new Element<K, V>(key, value);
            element.Next = rootElement;
            arr[elementIndex] = element;
            count++;
            MinMaxKey(key);
        }

        public bool Contains(K key)
        {
            try
            {
                Get(key);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            
        }

        public V Get(K key)
        {
            Element<K, V> rootElement = arr[GetElementIndex(key)];

            while (rootElement != null)
            {
                if (rootElement.Key.Equals(key))
                {
                    return rootElement.Value;
                }
                rootElement = rootElement.Next;
            }

            throw new KeyNotFoundException();
        }

        public V Remove(K key)
        {
            int index = GetElementIndex(key);
            Element<K, V> element = arr[index];
            Element<K, V> previousElement = null;

            while (element != null)
            {
                if (element.Key.Equals(key))
                    break;

                previousElement = element;
                element = element.Next;
            }

            if (element == null)
                throw new KeyNotFoundException();

            if (previousElement == null)
            {
                arr[index] = element.Next;
            }
            else
            {
                previousElement.Next = element.Next;
            }

            count--;
            return element.Value;
        }

        public IEnumerable<KeyValuePair<K, V>> Range(K minimum, K maximum)
        {
            return this[minimum, maximum];
        }

        private void MinMaxKey(K key)
        {

            if (key.GetHashCode() < minKey.GetHashCode())
            {
                minKey = key;
            }
            if (key.GetHashCode() > maxKey.GetHashCode())
            {
                maxKey = key;
            }
        }

        private int CompareTo(K key, K key2)
        {
            return key.GetHashCode().CompareTo(key2.GetHashCode());
        }

        private bool ApprovedToInsert(K key, K minKey, K maxKey)
        {

            return (CompareTo(key, minKey) >= 0) && (CompareTo(key, maxKey) <= 0);
        }

        public IEnumerable<KeyValuePair<K, V>> SortedRange(K minimum, K maximum)
        {
            return this[minimum, maximum].ToList().OrderBy(x => x.Key);
        }


    }
}
