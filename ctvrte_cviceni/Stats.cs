﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cv_04
{
    class Stats
    {
        public delegate void UpdatedStatsEventHandler(object sender, EventArgs e);
        private int correct;
        public int Correct { get { return correct; } set { correct = value;}}
        private int missed;
        public int Missed { get { return missed; } set { missed = value; } }
        private double accuracy;
        public double Accuracy { get { return accuracy; } set { accuracy = value; } }

        public event UpdatedStatsEventHandler UpdatedStats;

        public void Update(bool correctKey) {
            if (correctKey)
            {
                correct++;
            }
            else {
                missed++;
            }
            if(correct > 0)
            Accuracy = (double) Correct / (Correct + Missed);
            OnUpdatedStats();
        }

        private void OnUpdatedStats()
        {
            UpdatedStatsEventHandler handler = UpdatedStats;
            if (handler != null)
                handler(this, new EventArgs());
        }
        
    }
}
