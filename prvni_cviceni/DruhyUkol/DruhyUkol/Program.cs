﻿using System;

namespace DruhyUkol
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 97; i < 123; i++)
            {
                Console.WriteLine((char)i);
            }
            Console.WriteLine("------------------");
            int x = 97;
            while (x < 123)
            {
                Console.WriteLine((char)x);
                x++;
            }
            Console.WriteLine("------------------");
            x = 97;
            do
            {
                Console.WriteLine((char)x);
                x++;
            } while (x < 123);
            Console.ReadKey();
        }
    }
}
