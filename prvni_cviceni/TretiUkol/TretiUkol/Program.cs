﻿using System;

namespace TretiUkol
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                ulong birth;
                if (!ulong.TryParse(Console.ReadLine(), out birth))
                {
                    Console.WriteLine("neplatne rč, zadejte znovu");
                }
                else
                {
                    if (Woman(birth))
                    {
                        Console.WriteLine("jste žena");
                    }
                    else
                    {
                        Console.WriteLine("jste muž");
                    }
                }
                Console.ReadKey();
            }
           
        }

        public static Boolean Woman(ulong birth)
        {
            return int.Parse(birth.ToString().Substring(2, 1)) >= 4;
        }
    }
}
