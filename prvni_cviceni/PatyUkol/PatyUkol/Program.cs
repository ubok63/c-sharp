﻿using System;
using System.Text.Json;
using System.Xml;
using System.Text.RegularExpressions;

namespace PatyUkol
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            Console.WriteLine("zadejte lokaci pro zjištění počasí");
            String location = Console.ReadLine();
            System.Net.WebClient wc =  new System.Net.WebClient();
            Rootobject locationGPS = JsonSerializer.Deserialize<Rootobject>(wc.DownloadString("http://www.mapquestapi.com/geocoding/v1/address?key=GWIb2UVIgtnEDXYRCFoLsy3N1JJcrx4Q&location=" + location));
            string lat = locationGPS.results[0].locations[0].latLng.lat.ToString().Replace(",",".").Substring(0,5);
            string lng = locationGPS.results[0].locations[0].latLng.lng.ToString().Replace(",",".").Substring(0,5);
            string url = "https://api.met.no/weatherapi/locationforecast/2.0/compact?lat=" + lat + "&lon=" + lng;
            wc.Headers.Set("User-Agent", "Chrome/51.0.2704.103");
            WeatherForecast data = JsonSerializer.Deserialize<WeatherForecast>(wc.DownloadString(url));
            Console.WriteLine("Teď je " + data.properties.timeseries[0].data.instant.details.air_temperature + " stupňů");
            Console.WriteLine("Vlhkost: " + data.properties.timeseries[0].data.instant.details.relative_humidity + "%");
            Console.WriteLine("Rychlost větru: " + data.properties.timeseries[0].data.instant.details.wind_speed + " m/s");
            Console.WriteLine("Tlak: " + data.properties.timeseries[0].data.instant.details.air_pressure_at_sea_level + " hPa");
            Console.ReadKey();
        }
    }
}
