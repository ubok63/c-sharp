﻿using System;

namespace CtvrtyUkol
{
    class Program
    {
        static void Main(string[] args)
        {
            Random r = new Random();
            while (true)
            {
                int x = -1;
                int rng = r.Next(0, 100);
                int count = 0;
                Console.WriteLine("číslo bylo vygenerováno");
                while (true)
                {
                    if (!int.TryParse(Console.ReadLine(), out x))
                    {
                        Console.WriteLine("Není číslo");
                        count++;
                        if (OutOfTries(count))
                        {
                            Console.WriteLine("již nemáte žádný pokus");
                            break;
                        }
                        continue;
                    }
                    if (x < 0 || x > 100)
                    {
                        Console.WriteLine("Číslo nebylo nalezeno");
                    }
                    else if (x < rng)
                    {
                        Console.WriteLine("hledané číslo je větší");
                    }
                    else if(x > rng)
                    {
                        Console.WriteLine("hledané číslo je menší");
                    }
                    else
                    {
                        Console.WriteLine("uhádli jste");
                        break;
                    }
                    count++;
                    if(OutOfTries(count))
                    {
                        Console.WriteLine("již nemáte žádnný pokus");
                            break;
                    }
                }
 
                Console.WriteLine("chcete hrát znovu? ano - 1, ne - 0");
                String tmp = Console.ReadLine();
                if (tmp.Equals("1"))
                {
                    continue;
                }
                break;
            }
        }

        public static Boolean OutOfTries(int count)
        {
            return count == 10;
        }
    }
}
